# -*- coding: utf-8 -*-
"""
Created on Fri May  1 18:14:13 2020

@author: uia83721
"""

import abc

class Pizza( abc.ABC ):
    
    def __init__(self, name="", dough="", sauce="", toppings=None):
        self.name  = name
        self.dough = dough
        self.sauce = sauce
        self.toppings = toppings
        
    def prepare( self ):
        print ( "Preparing {}".format( self.name ) )
        print ( "Tossing dough..." )
        print ( "Adding sauce..." )
        print ( "Adding toppings..." )
        for topping in self.toppings:
            print ( " {}".format( topping ) )

    def bake( self ):
        print ( "Baje for 25 minutes at 350°" )
        
    def cut( self ):
        print ( "Cutting the pizza into diagonal slices" )            
        
    def box( self ):
        print ( "Place pizza in offical PizzaStore box" )

    def getName( self ):
        return self.name


class NYStyleCheesePizza( Pizza ):
    
    def __init__(self):
        super( NYStyleCheesePizza, self ).__init__("NY Style Sauce and Cheese Pizza", 
                                                 "Thin Crust Dough", 
                                                 "Marinara Sauce", 
                                                 ["Grated Reggiano Cheese", ])

class NYStylePepperoniPizza(Pizza):
    pass

class NYStyleClamPizza(Pizza):
    pass

class NYStyleVeggiePizza(Pizza):
    pass


class ChicagoStyleCheesePizza(Pizza):
    def __init__(self):
        super(ChicagoStyleCheesePizza, self).__init__("Chicago Style Deep Dish Cheese Pizza",
                                                      "Extra Thick Crust Dough", "Plum Tomato Sauce",
                                                      ["Shredded Mozzarella Cheese",])
    
    def cut(self):
        print("Cutting the pizza into square slices")

class ChicagoStylePepperoniPizza(Pizza):
    pass

class ChicagoStyleClamPizza(Pizza):
    pass

class ChicagoStyleVeggiePizza(Pizza):
    pass



class PizzaStore( abc.ABC ):
    
    def order_pizza( self, kind ):
        pizza = self.create_pizza( kind )
        
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()
        
        return pizza
    
    @staticmethod
    def create_pizza(kind):
        raise NotImplementedError()


class NYPizzaStore( PizzaStore ):
    
    @staticmethod
    def create_pizza(kind):
        
        pizza = None
        
        if kind == "cheese":
            pizza = NYStyleCheesePizza()
        elif kind == "pepperoni":
            pizza = NYStylePepperoniPizza()
        elif kind == "clam":
            pizza = NYStyleClamPizza()
        elif kind == "veggie":
            pizza = NYStyleVeggiePizza()
        
        return pizza

    
class ChicagoPizzaStore(PizzaStore):
    @staticmethod
    def create_pizza(kind):
        
        pizza = None
        
        if kind == "cheese":
            pizza = ChicagoStyleCheesePizza()
        elif kind == "pepperoni":
            pizza = ChicagoStylePepperoniPizza()
        elif kind == "clam":
            pizza = ChicagoStyleClamPizza()
        elif kind == "veggie":
            pizza = ChicagoStyleVeggiePizza()
        
        return pizza
    
    
if __name__ == "__main__":

    ny_store = NYPizzaStore()
    chicago_store = ChicagoPizzaStore()
    
    pizza = ny_store.order_pizza("cheese")
    print("Ethan ordered a {}.\n".format(pizza.name))

    pizza = chicago_store.order_pizza("cheese")
    print("Joel ordered a {}.\n".format(pizza.name))    