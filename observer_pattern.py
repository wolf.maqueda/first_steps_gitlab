# -*- coding: utf-8 -*-
"""
Created on Tue Apr 29 18:23:05 2020

@author: uia83721
"""
import abc
from random import randrange

class Observable( abc.ABC ):

    _state = None

    @abc.abstractmethod
    def addObserver( self, observer ):
        message = '{} should implement a method'.format( self.__class__.__name__)
        raise NotImplementedError( message )
        
    @abc.abstractmethod
    def deleteObserver( self, observer ):
        message = '{} should implement a method'.format( self.__class__.__name__)
        raise NotImplementedError( message )
        
    @abc.abstractmethod
    def notifyObservers( self ):
        message = '{} should implement a method'.format( self.__class__.__name__)
        raise NotImplementedError( message )
            
    def setChanged( self ):
        pass
        

class WheatherData( Observable ):

    _observers = []
    
    def addObserver( self, observer ):
        print ( "Observable: Added an Observer" )
        self._observers.append( observer )
        
    def deleteObserver( self, observer ):
        if observer in self._observers:
            self._observers.remove( observer )
        
    def notifyObservers( self ):
        print("Observable: Notifying observers...")
        for observer in self._observers:
            observer.update( self )        
    
    
    def a_wierd_logic(self):
        """
        Usually, the subscription logic is only a fraction of what an Observable can
        really do. Subjects commonly hold some important business logic, that
        triggers a notification method whenever something important is about to
        happen (or after it).
        """

        print( "Observable: I'm doing something important.")
        self._state = randrange(0, 10)

        print( "Observable: My state has just changed to: {0}".format( self._state ) )
        self.notifyObservers()    
    
    
        
class Observer( abc.ABC ):

    @abc.abstractmethod
    def update( self, observable ):
        message = '{} should implement a method'.format( self.__class__.__name__)
        raise NotImplementedError( message )

        
class StatisticsDisplay( Observer ):

    def update( self, observable ):
        if observable._state < 3:
            print ( "StatisticsDisplay: Reacted to the event" )


class ForecastDisplay( Observer ):

    def update(self, observable ):
        if observable._state == 0 or observable._state >= 2:
            print( "ForecastDisplay: Reacted to the event" )
            
            

if __name__ == "__main__":
    # The client code.

    wheatherStation = WheatherData()

    observer_a = StatisticsDisplay()
    wheatherStation.addObserver(observer_a)

    observer_b = ForecastDisplay()
    wheatherStation.addObserver(observer_b)

    wheatherStation.a_wierd_logic()
    wheatherStation.a_wierd_logic()

    wheatherStation.deleteObserver(observer_a)

    wheatherStation.a_wierd_logic()            
            