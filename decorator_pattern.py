# -*- coding: utf-8 -*-
"""
Created on Fri May  1 14:48:52 2020

@author: uia83721
"""
import abc

class Beverage( abc.ABC ):
    
    def __init__( self ):
        self.description = "Unknown Beverage"

    def getDescription( self ):
        return self.description

    @abc.abstractmethod
    def cost( self ):
        message = '{} should implement a method'.format( self.__class__.__name__)
        raise NotImplementedError( message )        
        
        
class CondimentDecorator( Beverage, abc.ABC ):

    @abc.abstractmethod    
    def getDescription( self ):
        message = '{} should implement a method'.format( self.__class__.__name__)
        raise NotImplementedError( message )                


class Espresso ( Beverage ):
    
    def __init__( self ):
        self.description = "Espresso"
        
    def cost( self ):
        return 1.99


class HouseBlend( Beverage ):
    
    def __init__( self ):
        self.description = "House Blend Coffee"
        
    def cost( self ):
        return 0.89

class DarkRoast( Beverage ):
    
    def __init__( self ):
        self.description = "Dark Roast Coffee"
        
    def cost( self ):
        return 0.99


class Decaf( Beverage ):
    
    def __init__( self ):
        self.description = "Decaf Coffee"
        
    def cost( self ):
        return 1.05    


class Mocha( CondimentDecorator ):
    
    def __init__( self, beverage ):
        self.beverage = beverage

    def getDescription( self ):
        return self.beverage.getDescription() + ", Mocha"
    
    def cost( self ):
        return self.beverage.cost() + 0.20
    
    
class SteamedMilk( CondimentDecorator ):
    
    def __init__( self, beverage ):
        self.beverage = beverage

    def getDescription( self ):
        return self.beverage.getDescription() + ", Steamed Milk"
    
    def cost( self ):
        return self.beverage.cost() + 0.10    


class Soy( CondimentDecorator ):
    
    def __init__( self, beverage ):
        self.beverage = beverage

    def getDescription( self ):
        return self.beverage.getDescription() + ", Soy"
    
    def cost( self ):
        return self.beverage.cost() + 0.15    
    
    
class Whip( CondimentDecorator ):
    
    def __init__( self, beverage ):
        self.beverage = beverage

    def getDescription( self ):
        return self.beverage.getDescription() + ", Whip"
    
    def cost( self ):
        return self.beverage.cost() + 0.10


if __name__ == "__main__":
    # The client code.
    beverage1 = Espresso()
    print ( beverage1.getDescription() + " $ {0}".format( beverage1.cost() ) )
    
    beverage2 = DarkRoast()
    beverage2 = Mocha( beverage2 )
    beverage2 = Mocha( beverage2 )
    beverage2 = Whip( beverage2 )
    print ( beverage2.getDescription() + " $ {0}".format( beverage2.cost() ) )