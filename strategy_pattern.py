# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 12:52:15 2020

@author: uia83721
"""
import abc

class FlyBehavior( abc.ABC ):
    
    @abc.abstractmethod
    def fly( self ):
        message = '{} should implement a method'.format( self.__class__.__name__)
        raise NotImplementedError( message )
        
        
class FlyWithWings( FlyBehavior ):

    def fly( self ):
        message = '{}.fly'.format( self.__class__.__name__ )
        print ( message )    


class FlyNoWay( FlyBehavior ):

    def fly( self ):
        message = '{}.fly'.format( self.__class__.__name__ )
        print ( message )

        
        
class QuackBehavior( abc.ABC ):
    
    @abc.abstractmethod
    def quack( self ):
        message = '{} should implement a method'.format( self.__class__.__name__)
        raise NotImplementedError( message )
        

class Quack( QuackBehavior ):

    def quack( self ):
        message = '{}.quack'.format( self.__class__.__name__ )
        print ( message )    


class Squeack( QuackBehavior ):

    def quack( self ):
        message = '{}.quack'.format( self.__class__.__name__ )
        print ( message )
        
        
class MuteQuack( QuackBehavior ):

    def quack( self ):
        message = '{}.quack'.format( self.__class__.__name__ )
        print ( message )
        
        
class Duck( object ):
    
    def __init__( self ):
        self.flybehavior = None#FlyBehavior()
        self.quackbehavior =  None #QuackBehavior()
        self.name = None
        
    def swim( self ):
        print ( "I'm swimming" )        
        
    def display( self ):
        print ( "show me" )
        
    def getName( self ):
        print ( "My name is {}".format( self.name ) )

    def setFlyBehavior( self, flybehavior ):
        self.flybehavior = flybehavior
    
    def setQuackBehavior( self, quackbehavior ):
        self.quackbehavior = quackbehavior
    
    def quack( self ):
        self.quackbehavior.quack()
    
    def fly ( self ):
        self.flybehavior.fly()
    

class MallardDuck( Duck ):
    def __init__( self ):
        self.name = "MallarDuckName"

class ReadHeadDuck( Duck ):
    def __init__( self ):
        self.name = "ReadHeadDuckName"

class RubberDuck( Duck ):
    def __init__( self ):
        self.name = "RubberDuckName"

class DecoyDuck( Duck ):
    def __init__( self ):
        self.name = "DecoyDuckName"


duckie1 = MallardDuck()
duckie1.setFlyBehavior ( FlyWithWings() )
duckie1.setQuackBehavior( MuteQuack() )
duckie1.getName()
duckie1.display()
duckie1.swim()
duckie1.quack()
duckie1.fly()

duckie2 = RubberDuck()
duckie2.setFlyBehavior ( FlyNoWay() )
duckie2.setQuackBehavior( Squeack() )
duckie2.getName()
duckie2.display()
duckie2.swim()
duckie2.quack()
duckie2.fly()